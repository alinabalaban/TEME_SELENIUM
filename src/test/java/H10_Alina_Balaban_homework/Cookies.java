package H10_Alina_Balaban_homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class Cookies {
    @FindBy(how = How.XPATH, using = "//H1[@class='text-center text-primary'][text()='The gibberish talking cookie']")
    private static WebElement PageTitle;

    @FindBy(how = How.ID, using = "set-cookie")
    private static WebElement SetCookieButton;

    @FindBy(how = How.ID, using = "delete-cookie")
    private static WebElement DeleteCookieButton;

    @FindBy(how = How.XPATH, using = "//H1[@id='cookie-value']")
    private static WebElement CookieValueString;

    public  void SetCookie(){
        Assert.assertEquals(PageTitle.getText(), "The gibberish talking cookie");
        SetCookieButton.isDisplayed();
        DeleteCookieButton.isDisplayed();
        SetCookieButton.click();
    }

    public  void DeleteCookie(){
        Assert.assertEquals(PageTitle.getText(), "The gibberish talking cookie");
        SetCookieButton.isDisplayed();
        DeleteCookieButton.isDisplayed();
        DeleteCookieButton.click();
    }
    public String getCookie(){
        Assert.assertEquals(PageTitle.getText(), "The gibberish talking cookie");
        SetCookieButton.isDisplayed();
        DeleteCookieButton.isDisplayed();
        String Cookietest = CookieValueString.getText();
        return Cookietest;
    }

}
