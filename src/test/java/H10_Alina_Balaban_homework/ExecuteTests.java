package H10_Alina_Balaban_homework;

import H9_Alina_Balaban_homework.General;
import H9_Alina_Balaban_homework.RegisterPage;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Set;

import static H9_Alina_Balaban_homework.General.*;

public class ExecuteTests extends General {
 @Test
 public static void TestViewCookies() {
  driver.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/cookie.html");
  Cookies Test1Cookie1 = PageFactory.initElements(driver, Cookies.class);
  Test1Cookie1.SetCookie();
  Set<Cookie> setOfCookies = driver.manage().getCookies();
  System.out.println("Found " + setOfCookies.size() + " cookies");
  for (Cookie c : setOfCookies)
  {
   System.out.println("Cookie name: " + c.getName());
    System.out.println("Cookie value: " + c.getValue());
   Assert.assertEquals(c.getName(),"gibberish");
   Assert.assertEquals(c.getValue(), Test1Cookie1.getCookie());
    System.out.println("Perfect match");
   }
  };


@Test
 public static void TestDeleteCookie() {
  driver.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/cookie.html");
  Cookies Test1Coockie1 = PageFactory.initElements(driver, Cookies.class);
  Test1Coockie1.DeleteCookie();
  Set<Cookie> setOfCookies = driver.manage().getCookies();
  System.out.println("Found " + setOfCookies.size() + " cookies");

   if(setOfCookies.size()>0){
    System.out.println("Test did not succeeded, coockies were not deleted.");
   }
   else {
    System.out.println("Test succeeded, all coockies were delete.");
   }


 }

}
