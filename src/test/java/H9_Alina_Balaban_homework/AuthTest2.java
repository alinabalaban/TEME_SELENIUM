package H9_Alina_Balaban_homework;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;


public class AuthTest2 extends General {


    @Test
    public void testModificat() {
        driver.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/auth.html");
        ///Se instantiata clasa ca sa imi incarce elementele
        RegisterPage Test1 = PageFactory.initElements(driver, RegisterPage.class);
        Test1.SetUserData("Alin", "Irina", "alina_irina@yahoo.com", "alina_irina");
        Test1.SetPassword("password1","password1");
        Test1.Register();
        WelcomePage Page2=PageFactory.initElements(driver, WelcomePage.class);
        Page2.VerifyLogin();
        Page2.Logout();
        Test1.VerifyFistPage();
    }
}
