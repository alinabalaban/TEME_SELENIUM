package H9_Alina_Balaban_homework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by algatu on 14/09/2017.
 */
public class WebBrowser {

    public enum Browsers {
        FIREFOX,
        CHROME,
        IE,
        HTMLUNIT
    }

    // This is the prefered option with enums and switch rather than nested ifs
    public static WebDriver getDriver(Browsers browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case IE: {
                System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer32.exe");
                return new InternetExplorerDriver();
            }
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");

                ChromeOptions options = new ChromeOptions();
                //options.setBinary("C:\\Users\\algatu\\AppData\\Local\\Vivaldi\\Application\\vivaldi.exe");
                options.addArguments("start-maximized");
                return new ChromeDriver(options);
            }
            case FIREFOX: {
                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                ProfilesIni profile = new ProfilesIni();
                FirefoxProfile myprofile = profile.getProfile("default");
                //System.out.println(myprofile.toJson());
                //File xpifile = new File("D://test.xpi");
                //myprofile.addExtension(xpifile);

                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability(FirefoxDriver.PROFILE, myprofile);
                dc.setCapability("browserName", "safari");
                //dc.setCapability(FirefoxDriver.BINARY, "D:\\Firefox31\\firefox.exe");

                return new FirefoxDriver(dc);
            }
            case HTMLUNIT: {
                return new HtmlUnitDriver();
            }
            default: {
                throw new Exception("Invalid browser");
            }
        }
    }

    // This works but is not recommended
    public static WebDriver getDriverString(String browser) throws Exception {
        if (browser.toUpperCase().compareTo("FIREFOX") == 0) {
            System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
            return new FirefoxDriver();
        } else {
            if (browser.toUpperCase().equals("CHROME")) {
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
                return new ChromeDriver();
            } else {
                if (browser.toUpperCase().equals("IE")) {
                    System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer32.exe");
                    return new InternetExplorerDriver();
                } else {
                    if (browser.toUpperCase().equals("HTMLUNIT")) {
                        return new HtmlUnitDriver();
                    } else {
                        throw new Exception("Invalid browser");
                    }
                }
            }
        }
    }

}
