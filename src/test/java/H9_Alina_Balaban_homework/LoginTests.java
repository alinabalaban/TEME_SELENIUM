package H9_Alina_Balaban_homework;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.awt.*;


public class LoginTests {

    public void mySleeper() {
        try {
            Thread.sleep(5000);
        } catch (Exception ex) {

        }
    }

    @Test
    public void PositiveTesting() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\Drivers\\chromedriver.exe");
        WebDriver driverChrome = new ChromeDriver();
        driverChrome.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/auth.html");
        WebElement RadioSelect = driverChrome.findElement(By.xpath("(//LABEL[@class='radio-inline'])[2]"));
        RadioSelect.click();
        WebElement FirstNameElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputFirstName']"));
        FirstNameElement.click();
        FirstNameElement.sendKeys("ALINA");
        WebElement LasttNameElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputLastName']"));
        LasttNameElement.click();
        LasttNameElement.sendKeys("Irina");
        WebElement EmailElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputEmail']"));
        EmailElement.click();
        EmailElement.sendKeys("alina_irina_b@yahoo.com");
        WebElement UsernameElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputUsername']"));
        UsernameElement.click();
        UsernameElement.sendKeys("alina_irina");
        WebElement PasswordElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputPassword']"));
        PasswordElement.click();
        PasswordElement.sendKeys("12345678");
        WebElement PasswordElement2 = driverChrome.findElement(By.xpath("//INPUT[@id='inputPassword2']"));
        PasswordElement2.click();
        PasswordElement2.sendKeys("12345678");
        Assert.assertEquals(FirstNameElement.getAttribute("value"), "ALINA");
        Assert.assertEquals(LasttNameElement.getAttribute("value"), "Irina");
        Assert.assertEquals(EmailElement.getAttribute("value"), "alina_irina_b@yahoo.com");
        Assert.assertEquals(UsernameElement.getAttribute("value"), "alina_irina");
        Assert.assertEquals(PasswordElement.getAttribute("value"), "12345678");
        Assert.assertEquals(PasswordElement2.getAttribute("value"), "12345678");


        WebElement SubmitElement = driverChrome.findElement(By.xpath("//BUTTON[@id='register-submit']"));
        SubmitElement.click();


        WebElement Welcome = driverChrome.findElement(By.id("greeting"));

        if (Welcome.isDisplayed()) {
            Assert.assertEquals(Welcome.getText(), "Welcome guest!");
            System.out.println("Inregistrat cu success si logat");
        } else {
            System.out.println("Inregistrarea nu s-a produs");
            System.exit(1);
        }
        WebElement Logout = driverChrome.findElement(By.xpath("//BUTTON[@id='logout-submit']"));
        Logout.click();
        mySleeper();
        WebElement RegistrationElement = driverChrome.findElement(By.xpath("//DIV[@class='panel-heading'][text()='Register']"));
        if (RegistrationElement.isDisplayed()) {
            System.out.println("Delogarea s-a produs cu success ");
        } else {
            System.out.println("Delogarea nu s-a putut produce");
            System.exit(1);
        }

        driverChrome.close();
    }

    @Test
    public void NegativeTesting() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\Drivers\\chromedriver.exe");
        WebDriver driverChrome = new ChromeDriver();
        driverChrome.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/auth.html");
        driverChrome.manage().window().maximize();
        WebElement RadioTitle = driverChrome.findElement(By.xpath("(//INPUT[@type='radio'])[1]"));
        RadioTitle.click();
        WebElement FirstNameElement = driverChrome.findElement(By.xpath("//INPUT[@id='inputFirstName']"));
        FirstNameElement.click();
        FirstNameElement.sendKeys("A");
        WebElement SubmitElement = driverChrome.findElement(By.xpath("//BUTTON[@id='register-submit']"));
        SubmitElement.click();
        Assert.assertEquals(FirstNameElement.getAttribute("value"), "A");
        WebElement Err1 = driverChrome.findElement(By.xpath("(//LI[text()='Invalid input. Please enter between 2 and 35 letters'][text()='Invalid input. Please enter between 2 and 35 letters'])[1]"));
//        WebElement Err2= driverChrome.findElement(By.xpath("(//LI[text()='Invalid input. Please enter between 2 and 35 letters'])[2]"));
        WebElement Err3 = driverChrome.findElement(By.xpath("(//LI[text()='Invalid input. Please enter a valid email address'])[1]"));
        WebElement Err4 = driverChrome.findElement(By.xpath("(//LI[text()='Invalid input. Please enter between 4 and 35 letters or numbers'])[1]"));
        WebElement Err5 = driverChrome.findElement(By.xpath("(//LI[text()='Invalid input. Please use a minimum of 8 characters'])[1]"));
        WebElement Err6 = driverChrome.findElement(By.xpath("//LI[text()='Please confirm your password']/self::LI"));


        if (Err1.isDisplayed()) {
            try {
                Assert.assertEquals(Err1.getText(), "Invalid input. Please enter between 2 and 35 letters");
            } catch (Throwable E) {
                System.out.println("Erorile nu sunt corecte");
                Assert.fail();
            }
            if (Err3.isDisplayed() || Err4.isDisplayed() || Err5.isDisplayed() || Err6.isDisplayed()) {
                try {
                    Assert.assertEquals(Err3.getText(), "Invalid input. Please enter a valid email address");
                    Assert.assertEquals(Err4.getText(), "Invalid input. Please enter between 4 and 35 letters or numbers");
                    Assert.assertEquals(Err5.getText(), "Invalid input. Please use a minimum of 8 characters");
                    Assert.assertEquals(Err6.getText(), "Please confirm your password");
                    System.out.println("Erorile sunt corecte");
                } catch (Throwable E) {
                    System.out.println("Erorile nu sunt corecte");
                    Assert.fail();

                }
            } else {
                System.out.println("Lipsesc erori sau sunt incorecte!");
                Assert.fail();
                System.exit(1);
            }
        } else {
            System.out.println("Lipsesc erori sau sunt incorecte!");
            System.exit(1);
        }

        driverChrome.close();
    }
}

