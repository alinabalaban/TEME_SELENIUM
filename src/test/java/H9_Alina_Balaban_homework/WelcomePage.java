package H9_Alina_Balaban_homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class WelcomePage {

    @FindBy(how = How.ID, using = "greeting")
    private static WebElement WelcomeText;

    @FindBy(how = How.ID, using = "logout-submit")
    private static WebElement LogOutButton;

    public void VerifyLogin(){
        Assert.assertEquals(WelcomeText.getText(), "Welcome guest!");
        LogOutButton.isDisplayed();

    }

    public void Logout(){
        LogOutButton.click();
    }
}
