package H9_Alina_Balaban_homework;

import org.apache.xpath.SourceTree;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class RegisterPage {
    @FindBy(how = How.XPATH, using = "//H1[@class='text-center text-primary'][text()='Authentication']")
    private static WebElement Authentification;

    @FindBy(how = How.XPATH, using = "(//INPUT[@type='radio'])[1]")
    private static WebElement RadioTitle1;

    @FindBy(how = How.XPATH, using = "(//LABEL[@class='radio-inline'])[2]")
    private static WebElement RadioTitle2;

    @FindBy(how = How.ID, using = "inputFirstName")
    private static WebElement FirstName;

    @FindBy(how = How.ID, using = "inputLastName")
    private static WebElement LasttName;

    @FindBy(how = How.ID, using = "inputEmail")
    private static WebElement Email;

    @FindBy(how = How.ID, using = "inputUsername")
    private static WebElement Username;

    @FindBy(how = How.ID, using = "inputPassword")
    private static WebElement Password;

    @FindBy(how = How.ID, using = "inputPassword2")
    private static WebElement Password2;

    @FindBy(how = How.ID, using = "register-submit")
    private static WebElement RegisterButton;

    @FindBy(how = How.ID, using = "bday")
    private static WebElement Birthday;

    @FindBy(how = How.ID, using = "bmonth")
    private static WebElement Birthmonth;

    @FindBy(how = How.ID, using = "byear")
    private static WebElement Birthyear;

    @FindBy(how = How.XPATH, using = "//INPUT[@type='checkbox']")
    private static WebElement SubscribeNews;

    public static void SetUserGender(int i) {
        if (i == 1) {
            RadioTitle1.click();
        } else {
            RadioTitle2.click();
        }

    }

    public void SetUserData(String FN, String LN, String Em, String Un) {
        FirstName.click();
        FirstName.sendKeys(FN);
        LasttName.click();
        LasttName.sendKeys(LN);
        Email.click();
        Email.sendKeys(Em);
        Username.click();
        Username.sendKeys(Un);

    }

    public void SetPassword(String Parola1, String Parola2) {
        Password.click();
        Password.sendKeys(Parola1);
        Password2.click();
        Password2.sendKeys(Parola2);


    }

public void Register(){
        RegisterButton.click();

}
public void VerifyFistPage(){
    Authentification.isDisplayed();
    RadioTitle1.isDisplayed();
    RadioTitle2.isDisplayed();
    FirstName.isDisplayed();
    LasttName.isDisplayed();
    Username.isDisplayed();
    Email.isDisplayed();


};
}
;






